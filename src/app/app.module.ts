import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module'
import { SLOGIN } from './services/social-login-providers'

import { AppComponent } from './app.component'
import { NavbarComponent } from './components/navbar/navbar.component'
import { HomeComponent } from './components/home/home.component'
import { GalleryComponent } from './components/gallery/gallery.component'
import { DuelsComponent } from './components/duels/duels.component'
import { RankingComponent } from './components/ranking/ranking.component'
import { ChatComponent } from './components/chat/chat.component'
import { LoginFormComponent } from './components/login-form/login-form.component'
import { ProfileComponent } from './components/profile/profile.component'
import { ProfileSettingComponent } from './components/profile-setting/profile-setting.component'

import { ImageCropperComponent } from 'ngx-img-cropper'
import { ImageCropperModule } from 'ngx-image-cropper'

import { NotifierModule } from 'angular-notifier';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover'

import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login'
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login'
import { ProfileInfoComponent } from './components/profile-info/profile-info.component'
import { LevelPipe } from './pipes/level/level.pipe'
import { ClassPipe } from './pipes/class/class.pipe';
import { CarouselLastvisitsComponent } from './components/carousel-lastvisits/carousel-lastvisits.component'
import { ImageUploaderComponent } from './components/image-uploader/image-uploader.component'
import { HeaderUploaderComponent } from './components/header-uploader/header-uploader.component'
import { AvatarUploaderComponent } from './components/avatar-uploader/avatar-uploader.component'

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(SLOGIN.googleProvider)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(SLOGIN.facebookProvider)
  }
])

export function provideConfig() {
  return config
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    GalleryComponent,
    DuelsComponent,
    RankingComponent,
    ChatComponent,
    LoginFormComponent,
    ProfileComponent,
    ProfileSettingComponent,
    ProfileInfoComponent,
    ImageCropperComponent,
    CarouselLastvisitsComponent,
    LevelPipe,
    ClassPipe,
    ImageUploaderComponent,
    HeaderUploaderComponent,
    AvatarUploaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ImageCropperModule,
    NotifierModule,
    SocialLoginModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    })
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
