import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'class'
})
export class ClassPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case ('Illustration'):
        return 'witch'
      case ('Photography'):
        return 'thief'
      case ('Watcher'):
        return 'warrior'
    }
  }

}