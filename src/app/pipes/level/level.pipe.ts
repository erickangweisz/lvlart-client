import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'level'
})
export class LevelPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let experience = value
    switch (true) {
      case (experience >= 0 && experience <= 50):
        return 1
      case (experience > 50 && experience <= 200):
        return 2
      case (experience > 200 && experience <= 500):
        return 3
      default:
        return 100
    }
  }

}
