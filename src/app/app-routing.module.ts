import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from '../app/guards/auth.guard'

import { HomeComponent } from './components/home/home.component'
import { GalleryComponent } from './components/gallery/gallery.component'
import { DuelsComponent } from './components/duels/duels.component'
import { RankingComponent } from './components/ranking/ranking.component'
import { ChatComponent } from './components/chat/chat.component'
import { ProfileComponent } from './components/profile/profile.component'
import { ProfileSettingComponent } from './components/profile-setting/profile-setting.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'duels', component: DuelsComponent },
  { path: 'ranking', component: RankingComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile-setting', component: ProfileSettingComponent, canActivate: [AuthGuard] }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
