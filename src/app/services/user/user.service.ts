import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { GLOBAL } from '../global'
import { User } from '../../models/user'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url
  }

  createUser(user) {
    return this.http.post(this.url + 'register', user)
  }

  updateUser(user: User) {
    return this.http.put(this.url + 'update-user/' + user._id, user, httpOptions)
  }

  updateState(params: any) {
    let body = { userId: params.userId, state: params.state }
    return this.http.put(this.url + 'update-state/' + params.userId, body, httpOptions)
  }

  changePassword(params: any) {
    let body = { newPassword: params.newPassword, currentPassword: params.currentPassword }
    return this.http.put(this.url + 'change-password/' + params.userId, body, httpOptions)
  }

  setActiveStatus(params: any) {
    let body = { userId: params.userId, active: params.active }
    return this.http.put(this.url + 'set-active-status/' + params.userId, body, httpOptions)
  }

  setStateOfSocialNetworkButtons(params: any) {
    let body: any
    if (params.hasOwnProperty('active_facebook')) {
      body = {
        userId: params.userId,
        active_facebook: params.active_facebook
      }
    } else if (params.hasOwnProperty('active_twitter')) {
      body = {
        userId: params.userId,
        active_twitter: params.active_twitter
      }
    } else if (params.hasOwnProperty('active_deviantart')) {
      body = {
        userId: params.userId,
        active_deviantart: params.active_deviantart
      }
    }
    return this.http.put(this.url + 'set-active-socialnetworks/' + params.userId, body, httpOptions)
  }

  getUser(userId: string) {
    return this.http.get(this.url + 'user/' + userId, httpOptions)
  }

  checkUsernameIsRegistered(username) {
    return this.http.post(this.url + 'check-username', { body: username })
  }

  checkEmailIsRegistered(email) {
    return this.http.post(this.url + 'check-email', { body: email })
  }

  checkBirthday(birthday) {
    return this.http.post(this.url + 'check-birthday', { body: birthday })
  }

  recoverPassword(email) {
    return this.http.post(this.url + 'recover-password', { body: email })
  }

}
