import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'
import { GLOBAL } from '../global'
import { Router } from '@angular/router'

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string) {
    return this.http.post<any>(GLOBAL.url + "login", { email, password })
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user.user))
          localStorage.setItem('token', user.token)
        }
        return user
    }))
  }

  logout() {
    localStorage.clear()
    this.router.navigate(['/home'])
  }

}
