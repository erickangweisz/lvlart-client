export class User {
    constructor(
        public _id: string,
        public username: string,
        public firstname: string,
        public lastname: string,
        public password: string,
        public state: string,
        public birthday: Date,
        public email: string,
        public category: string,
        public experience: string,
        public img_head: string,
        public avatar: string,
        public profile_facebook: string,
        public profile_twitter: string,
        public profile_deviantart: string,
        public active_facebook: boolean,
        public active_twitter: boolean,
        public active_deviantart: boolean
    ) {}
}
