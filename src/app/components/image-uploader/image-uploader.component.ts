import { Component, OnInit, ViewChild } from '@angular/core'
import { ImageCroppedEvent } from 'ngx-image-cropper'
import { ImageCropperComponent } from 'ngx-img-cropper'
import { NotifierService } from 'angular-notifier'
import { UserService } from '../../services/user/user.service'
import { User } from '../../models/user'
import { GLOBAL } from '../../services/global'

declare var $: any

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.sass']
})
export class ImageUploaderComponent implements OnInit {

  private token: string
  private user: User

  private readonly notifier: NotifierService

  private filesToUpload: Array<File>
  private croppedImage: any =  ''
  private imageChangedEvent: any = ''
  @ViewChild('cropper', undefined)
  private cropper: ImageCropperComponent
  private imageURL: string

  private block: any
  private contentType: any
  private realData: any
  private blob: Blob

  constructor(private notifierService: NotifierService,
              private userService: UserService) {
    this.notifier = notifierService
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.token = localStorage.getItem('token')
    this.imageURL = GLOBAL.url
  }

  ngOnInit() {}

  uploadImage() {
    if (!this.filesToUpload) {
      this.notifier.show({
        type: 'error',
        message: 'YOU HAVE NOT UPLOADED ANY IMAGES!'
      })
    } else {
      this.makeFileRequest(GLOBAL.url + 'upload-header/' + this.user._id, [], this.filesToUpload)
      .then((result: any) => {
        this.user.img_head = result.img_head
        localStorage.setItem('currentUser', JSON.stringify(this.user))
        location.reload()
      })
      $('#modalUploadImage').modal('toggle')
    }
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData()
      let xhr = new XMLHttpRequest()

      for (let i=0; i<files.length; i++) {
        formData.append('img_head', files[i], files[i].name)
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response))
          } else {
            reject(xhr.response)
          }
        }
      }
      xhr.open('POST', url, true)
      xhr.setRequestHeader('Authorization', this.token)
      xhr.send(formData)
    })
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64
    this.imageURL = this.croppedImage
    this.transformBlobToUpload()
  }

  transformBlobToUpload() {
    let count = 0
    this.block = this.imageURL.split(';')
    this.contentType = this.block[0].split(":")[1]
    this.realData = this.block[1].split(",")[1]
    this.blob = this.convertB64ToBlob(this.realData, this.contentType, '')
    let file = new File([this.blob], this.user._id + count + '.png', { type: this.blob.type })
    count++
    let files = new Array<File>(file)
    this.filesToUpload = files
  }

  convertB64ToBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || ''
    sliceSize = sliceSize || 512

    let byteCharacters = atob(b64Data)
    let byteArrays = []

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize)
      let byteNumbers = new Array(slice.length)

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i)
      }
      let byteArray = new Uint8Array(byteNumbers)
      byteArrays.push(byteArray)
    }
    let blob = new Blob(byteArrays, { type: contentType })
    return blob
  }

  imageLoaded() {}
  loadImageFailed() {}

}
