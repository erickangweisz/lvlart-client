import { Component, OnInit } from '@angular/core'
import { UserService } from '../../services/user/user.service'
import { AuthenticationService } from '../../services/authentication/authentication.service'
import { NavbarService } from '../../services/navbar/navbar.service'
import { User } from '../../models/user'
import { first } from 'rxjs/operators'
import { Router } from '@angular/router'

import { AuthService, SocialUser } from 'angularx-social-login'
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login'

declare var $: any

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.sass'],
  providers: [ AuthService ]
})
export class LoginFormComponent implements OnInit {

  private user: SocialUser

  private categories = ['Illustration', 'Photography', 'Watcher']

  private error =  'REGISTER ERROR'
  private termsAndConditionsAreAccepted: boolean = false

  private $divForms: any

  constructor(private userService: UserService, private authService: AuthenticationService,
    private nav: NavbarService, private router: Router, private authSocialService: AuthService) {}

  ngOnInit() {
    this.initAuthSocial()
    this.getLoginFormToHandleAnimations()
    this.validateForms()
    this.$divForms= $('#div-forms')
  }

  initAuthSocial() {
    this.authSocialService.authState.subscribe((authenticatedUser) => {
      this.user = authenticatedUser
    })
  }

  signInWithGoogle(): void {
    this.authSocialService.signIn(GoogleLoginProvider.PROVIDER_ID)
  }

  signInWithFB(): void {
    this.authSocialService.signIn(FacebookLoginProvider.PROVIDER_ID)
  }

  signOut(): void {
    this.authSocialService.signOut()
  }

  getLoginFormToHandleAnimations() {
    let $formLogin = $('#login-form')
    let $formLost = $('#lost-form')
    let $formRegister = $('#register-form')

    $('#login_register_btn').click( () => { this.modalAnimate($formLogin, $formRegister) })
    $('#register_login_btn').click( () => { this.modalAnimate($formRegister, $formLogin) })
    $('#login_lost_btn').click( () => { this.modalAnimate($formLogin, $formLost) })
    $('#lost_login_btn').click( () => { this.modalAnimate($formLost, $formLogin) })
    $('#lost_register_btn').click( () => { this.modalAnimate($formLost, $formRegister) })
    $('#register_lost_btn').click( () => { this.modalAnimate($formRegister, $formLost) })
  }

  modalAnimate ($oldForm: any, $newForm: any) {
    let $oldH = $oldForm.height()
    let $newH = $newForm.height()
    const $modalAnimateTime = 300
    
    this.$divForms.css('height', $oldH)
    $oldForm.fadeToggle($modalAnimateTime, () => {
      this.$divForms.animate({height: $newH}, $modalAnimateTime, () => {
        $newForm.fadeToggle($modalAnimateTime)
      })
    })
  }

  changeMessagesOfLoginForm($divTag, $textTag, $divClass, $msgText) {
    this.fadeOutMessage($textTag, $msgText)
    
    if ($divClass === '') {
      $divTag.removeClass('error')
      $divTag.removeClass('success')
    }
    $divTag.addClass($divClass)
  }

  fadeOutMessage ($msgId, $msgText) {
    let $msgAnimateTime = 150
    $msgId.fadeOut($msgAnimateTime, () => {
      $($msgId).text($msgText).fadeIn($msgAnimateTime)
    })
  }
  
  validateForms() {
    this.validatorLoginForm()
    this.validatorRecoverPasswordForm()
    this.validatorRegisterForm()
  }

  validatorLoginForm() {
    $("#btnLogin").click((event) => {
      let form = (<HTMLInputElement>document.getElementById("login-form"))

      if (form[0].checkValidity() === false || form[1].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
      } else {
        let $inputEmail=$('#login_email').val().toString()
        let $inputPassword=$('#login_password').val().toString()

        this.authService.login($inputEmail, $inputPassword).pipe(first())
        .subscribe(data => {
          this.changeMessagesOfLoginForm($('#div-login-msg'), $('#text-login-msg'), "success", "LOGIN SUCCESSFUL!!")
          this.router.navigate(['home'])
          this.nav.show()
          
          setTimeout(() => { $("#btnProfile").addClass("animated rubberBand") }, 400)
          $('#login-modal').modal('toggle')
          setTimeout(() => { this.changeMessagesOfLoginForm($('#div-login-msg'), $('#text-login-msg'), "", "TYPE YOUR E-MAIL AND PASSWORD") }, 2000)
        },
        err => {
          if (err.status === 404) {
            this.changeMessagesOfLoginForm($('#div-login-msg'), $('#text-login-msg'), "error", "E-MAIL/PASSWORD INCORRECT")
            $('#login_email').val('')
            $('#login_password').val('')
          }
        })
      }
      form.classList.add('was-validated')
    })
  }

  validatorRecoverPasswordForm() {
    $("#btnLost").click((event) => {
      let form = (<HTMLInputElement>document.getElementById("lost-form"))

      if (form[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
      } else {
        let $inputEmail=$('#lost_email').val()
        this.userService.recoverPassword($inputEmail).pipe(first())
        .subscribe(data => {
          this.changeMessagesOfLoginForm($('#div-lost-msg'), $('#text-lost-msg'), "success", "We have sent you an email with your password!!") 
        })
      }
      form.classList.add('was-validated')
    })
  }

  validatorRegisterForm() {
    $("#btnRegister").click((event) => {
      let form = (<HTMLInputElement>document.getElementById("register-form"))

      if (form[0].checkValidity() === false || form[1].checkValidity() === false ||
          form[2].checkValidity() === false || form[3].checkValidity() === false ||
          form[4].checkValidity() === false || form[5].checkValidity() === false ||
          form[6].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
      } else {
        let $inputUsername=$('#register_username').val().toString()
        let $inputFirstname=$('#register_firstname').val().toString()
        let $inputLastname=$('#register_lastname').val().toString()
        let $inputEmail=$('#register_email').val().toString()
        let $inputPassword=$('#register_password').val().toString()
        let $inputBirthday=$('#register_birthday').val().toString()
        let $inputCategory=$('#register_category').val().toString()
        let category = $inputCategory.substring(3)

        let user = new User('', $inputUsername, $inputFirstname, $inputLastname, $inputPassword, null, $inputBirthday, $inputEmail, category, null, null, null, null, null, null, true, true, true)   
        this.userService.checkUsernameIsRegistered($inputUsername).pipe(first())
        .subscribe(
          data => {
            if (data['username']) { // if user exist
              this.error = 'The username ' + $inputUsername + ' is used'
              this.changeMessagesOfLoginForm($('#div-register-msg'), $('#text-register-msg'), 'error', this.error)
              $('#register_username').val('')
            } else {
              this.userService.checkEmailIsRegistered($inputEmail).pipe(first())
              .subscribe(
                data => {
                  if (data['email']) {// if email exist
                    this.error = 'The email ' + $inputEmail + ' is used'
                    this.changeMessagesOfLoginForm($('#div-register-msg'), $('#text-register-msg'), 'error', this.error)
                    $('#register_email').val('')
                  } else {
                    this.userService.checkBirthday($inputBirthday).pipe(first()) //  the year is checked in the server
                    .subscribe(data => {
                      let birthdayFaked = data['birthday']
                      if (birthdayFaked) {
                        this.changeMessagesOfLoginForm($('#div-register-msg'), $('#text-register-msg'), 'erro', "DON'T BE EVIL!!")
                        $('#register_birthday').val('')
                      } else {
                        this.userService.createUser(user).pipe(first()) // user is created
                        .subscribe(
                          data => {
                            this.changeMessagesOfLoginForm($('#div-register-msg'), $('#text-register-msg'), 'success', 'REGISTER SUCCESSFUL!! GO TO LOGIN')
                          },
                          error => {
                            this.error = error['error'].message
                            this.changeMessagesOfLoginForm($('#div-register-msg'), $('#text-register-msg'), 'error', this.error)
                          })
                        }
                    })
                  }
                }
              )
            }
          }
        )
      }
      form.classList.add('was-validated')
    })
  }

}