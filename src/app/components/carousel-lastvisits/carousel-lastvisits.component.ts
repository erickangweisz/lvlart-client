import { Component, OnInit } from '@angular/core'

declare var Swiper: any

@Component({
  selector: 'app-carousel-lastvisits',
  templateUrl: './carousel-lastvisits.component.html',
  styleUrls: ['./carousel-lastvisits.component.sass']
})
export class CarouselLastvisitsComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    this.initSwiper()
  }

  initSwiper() {
    const swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      loop: true,
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    })
  }

}
