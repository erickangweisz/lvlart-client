import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselLastvisitsComponent } from './carousel-lastvisits.component';

describe('CarouselLastvisitsComponent', () => {
  let component: CarouselLastvisitsComponent;
  let fixture: ComponentFixture<CarouselLastvisitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselLastvisitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselLastvisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
