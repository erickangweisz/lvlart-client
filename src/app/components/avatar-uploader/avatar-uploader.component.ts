import { Component, OnInit } from '@angular/core'
import { NotifierService } from 'angular-notifier'
import { ImageCroppedEvent } from 'ngx-image-cropper'
import { GLOBAL } from '../../services/global'
import { User } from '../../models/user'

declare let $: any

@Component({
  selector: 'app-avatar-uploader',
  templateUrl: './avatar-uploader.component.html',
  styleUrls: ['./avatar-uploader.component.sass']
})
export class AvatarUploaderComponent implements OnInit {

  private token: string
  private user: User
  private userId: string

  public imageChangedEvent: any = ''
  private filesToUpload: Array<File>
  private croppedImage: any = ''
  private imageURL: string

  readonly notifier: NotifierService

  constructor(notifierService: NotifierService) {
    this.notifier = notifierService
  }

  ngOnInit() {
    this.token = localStorage.getItem('token')
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.userId = this.user._id
  }

  uploadImage() {
    if (!this.filesToUpload) {
      this.notifier.show({
        type: 'error',
        message: 'YOU HAVE NOT UPLOADED ANY IMAGES!'
      })
    } else {
      this.makeFileRequest(GLOBAL.url + 'upload-avatar/' + this.userId, [], this.filesToUpload)
        .then((result: any) => {
          this.user.avatar = result.avatar
          localStorage.setItem('currentUser', JSON.stringify(this.user))
          
          this.notifier.show({
            type: 'success',
            message: 'CORRECTLY CHANGED!'
          })
        })
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64
    this.imageURL = this.croppedImage

    $('#img-avt').addClass('img-thumbnail')
    this.transformBlobToUpload()
  }

  private makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData()
      let xhr = new XMLHttpRequest()

      for (let i = 0; i < files.length; i++) {
        formData.append('avatar', files[i], files[i].name)
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response))
          } else {
            reject(xhr.response)
          }
        }
      }
      xhr.open('POST', url, true)
      xhr.setRequestHeader('Authorization', this.token)
      xhr.send(formData)
    })
  }

  private transformBlobToUpload() {
    let count = 0
    let block: any
    let contentType
    let realData: any
    let blob: Blob

    block = this.imageURL.split(';')
    contentType = block[0].split(":")[1]
    realData = block[1].split(",")[1]
    blob = this.b64toBlob(realData, contentType, '')
    
    let file = new File([blob], this.userId + count + '.png', { type: blob.type })
    let files = new Array<File>(file)
    this.filesToUpload = files
    count++
  }

  private b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || ''
    sliceSize = sliceSize || 512

    let byteCharacters = atob(b64Data)
    let byteArrays = []

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize)
      let byteNumbers = new Array(slice.length)

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i)
      }
      let byteArray = new Uint8Array(byteNumbers)
      byteArrays.push(byteArray)
    }
    let blob = new Blob(byteArrays, { type: contentType })
    return blob
  }

  private imageLoaded() {}
  private loadImageFailed() {}

}
