import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUploaderComponent } from './header-uploader.component';

describe('HeaderUploaderComponent', () => {
  let component: HeaderUploaderComponent;
  let fixture: ComponentFixture<HeaderUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
