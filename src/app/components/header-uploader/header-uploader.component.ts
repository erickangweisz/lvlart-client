import { Component, OnInit, ViewChild } from '@angular/core'
import { ImageCroppedEvent } from 'ngx-image-cropper'
import { ImageCropperComponent } from 'ngx-img-cropper'
import { NotifierService } from 'angular-notifier'
import { UserService } from '../../services/user/user.service'
import { User } from '../../models/user'
import { GLOBAL } from '../../services/global'

declare let $: any

@Component({
  selector: 'app-header-uploader',
  templateUrl: './header-uploader.component.html',
  styleUrls: ['./header-uploader.component.sass']
})
export class HeaderUploaderComponent implements OnInit {

  private token: string
  private user: User

  private filesToUpload: Array<File>
  private croppedImage: any =  ''
  imageChangedEvent: any = ''
  @ViewChild('cropper', undefined)
  cropper: ImageCropperComponent
  imageURL: string

  private readonly notifier: NotifierService

  constructor(private notifierService: NotifierService,
              private userService: UserService) {
    this.notifier = notifierService
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.token = localStorage.getItem('token')
    this.imageURL = GLOBAL.url
  }

  uploadImage() {
    if (!this.filesToUpload) {
      this.notifier.show({
        type: 'error',
        message: 'YOU HAVE NOT UPLOADED ANY IMAGES!'
      })
    } else {
      this.makeFileRequest(GLOBAL.url + 'upload-header/' + this.user._id, [], this.filesToUpload)
      .then((result: any) => {
        this.user.img_head = result.img_head
        localStorage.setItem('currentUser', JSON.stringify(this.user))
        location.reload()
      })
      $('#modalUploadImage').modal('toggle')
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64
    this.imageURL = this.croppedImage
    this.transformBlobToUpload()
  }

  private makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData()
      let xhr = new XMLHttpRequest()

      for (let i=0; i<files.length; i++) {
        formData.append('img_head', files[i], files[i].name)
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response))
          } else {
            reject(xhr.response)
          }
        }
      }
      xhr.open('POST', url, true)
      xhr.setRequestHeader('Authorization', this.token)
      xhr.send(formData)
    })
  }

  private transformBlobToUpload() {
    let count = 0
    let block: any
    let contentType: any
    let realData: any
    let blob: Blob
    block = this.imageURL.split(';')
    contentType = block[0].split(":")[1]
    realData = block[1].split(",")[1]
    blob = this.convertB64ToBlob(realData, contentType, '')
    let file = new File([blob], this.user._id + count + '.png', { type: blob.type })
    count++
    let files = new Array<File>(file)
    this.filesToUpload = files
  }

  private convertB64ToBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || ''
    sliceSize = sliceSize || 512

    let byteCharacters = atob(b64Data)
    let byteArrays = []

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize)
      let byteNumbers = new Array(slice.length)

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i)
      }
      let byteArray = new Uint8Array(byteNumbers)
      byteArrays.push(byteArray)
    }
    let blob = new Blob(byteArrays, { type: contentType })
    return blob
  }

  imageLoaded() {}
  loadImageFailed() {}

}
