import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '../../models/user'
import { GLOBAL } from '../../services/global'
import { UserService } from '../../services/user/user.service'

declare var $: any

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass'],
  providers: [ UserService ]
})
export class ProfileComponent implements OnInit {
  
  private token: string
  private user: User

  private imageURL: string
  private newState: string

  constructor(private userService: UserService, 
              private router: Router) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.token = localStorage.getItem('token')

    this.imageURL = GLOBAL.url
    this.inputStateEditable()
  }

  inputStateEditable() {
    $('body').on('click', '[data-editable]', function () {
      var $el = $(this)
      var $input = $('<input id="input-state" class="float-right" style="margin-right: 4%;"/>').val($el.text());
      
      $el.replaceWith($input)
      $input.one('blur', spanChangesAnInput).focus()

      function spanChangesAnInput() {
        var $span = $('<span id="input-state" data-editable class="float-right" style="margin-right: 4%; color: white; font-size: 1.4vw; line-height: initial"/>').text($input.val());
        $input.replaceWith($span)

        this.newState = $input.val().toString()

        let body = {
          userId: this.user._id,
          state: this.newState
        }
      }
    })
  }

  shakeAvatar(event) {
    $('#' + event.target.id).toggleClass("animated shake")
  }

  navigateToProfileSetting() {
    this.router.navigate(['profile-setting'])
  }

}
