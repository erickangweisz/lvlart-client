import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { CarouselLastvisitsComponent } from '../carousel-lastvisits/carousel-lastvisits.component';
import { ProfileInfoComponent } from '../profile-info/profile-info.component';
import { HeaderUploaderComponent } from '../header-uploader/header-uploader.component';
import { ImageUploaderComponent } from '../image-uploader/image-uploader.component';
import { ClassPipe } from '../../pipes/class/class.pipe';
import { LevelPipe } from '../../pipes/level/level.pipe';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        CarouselLastvisitsComponent,
        ProfileInfoComponent,
        HeaderUploaderComponent,
        ImageUploaderComponent,

        ClassPipe,
        LevelPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
