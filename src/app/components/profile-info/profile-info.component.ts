import { Component, OnInit, Input } from '@angular/core'
import { User } from '../../models/user'
import { GLOBAL } from '../../services/global'

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.sass']
})
export class ProfileInfoComponent implements OnInit {

  private token: string
  private user: User
  private userId: string

  private urlImage: string

  @Input() experience: string

  constructor() {
    this.token = localStorage.getItem('token')
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.userId = this.user._id.toString()
    this.experience = this.user.experience

    this.getCategoryImage()
  }

  ngOnInit() {}

  getCategoryImage() {
    switch (this.user.category) {
      case 'Illustration':
        this.urlImage = GLOBAL.url + 'images/user-illustrator'
        break
      case 'Photography':
        this.urlImage = GLOBAL.url + 'images/user-photography'
        break
      case 'Watcher':
        this.urlImage = GLOBAL.url + 'images/user-watcher'
        break
    }
  }

  getExperience() {
    return this.experience + '%'
  }

}
