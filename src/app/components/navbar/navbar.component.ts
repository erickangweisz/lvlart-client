import { Component, OnInit } from '@angular/core'
import { AuthenticationService } from '../../services/authentication/authentication.service'
import { NavbarService } from '../../services/navbar/navbar.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
  providers: [ NavbarService ]
})
export class NavbarComponent implements OnInit {

  private title: string
  private currentUser: any

  constructor(private authService: AuthenticationService, 
                        private nav: NavbarService) {
    this.title = 'LVL-ART'
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
    if (this.currentUser !== null) this.nav.show()
  }

  logout() {
    this.nav.hide()
    this.authService.logout()
  }

}