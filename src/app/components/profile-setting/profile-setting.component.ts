import { Component, OnInit, ViewChild } from '@angular/core'
import { NotifierService } from 'angular-notifier'
import { Router } from '@angular/router'
import { ImageCropperComponent } from 'ngx-img-cropper'
import { User } from '../../models/user'

import { UserService } from '../../services/user/user.service'

import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'

declare var $: any

@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.sass']
})
export class ProfileSettingComponent implements OnInit {

  private popoverTitle: string = 'DEACTIVATE YOUR ACCOUNT'
  private popoverMessage: string = 'Why you wanna leave us? Are you sure? Think about it...'
  private confirmClicked: boolean = false
  private cancelClicked: boolean = false

  private token: string
  private user: User
  private userId: string
  private username: string
  private firstname: string
  private lastname: string
  private email: string
  private birthday: Date
  private avatar: string

  private profile_facebook: string
  private profile_twitter: string
  private profile_deviantart: string
  private active_facebook: boolean
  private active_twitter: boolean
  private active_deviantart: boolean

  private currentPassword: string = ''
  private newPassword: string = ''
  private passwordRepeated: string = ''

  private editForm: FormGroup
  private changePassForm: FormGroup

  @ViewChild('cropper', undefined)
  private cropper: ImageCropperComponent

  readonly notifier: NotifierService

  constructor(public router: Router,
              private formBuilder: FormBuilder, 
              private userService: UserService,
              public notifierService: NotifierService) {
    this.notifier = notifierService
  }

  ngOnInit() {
    this.token = localStorage.getItem('token')

    this.initCurrentUser()
    this.initEditForm()
    this.initChangePassForm()
  }

  initCurrentUser() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.userId = this.user._id
    this.birthday = new Date(Date.parse(this.user.birthday.toString()))
    this.active_facebook = JSON.parse(this.user.active_facebook.toString())
    this.active_twitter = JSON.parse(this.user.active_twitter.toString())
    this.active_deviantart = JSON.parse(this.user.active_deviantart.toString())
  }

  initEditForm() {
    this.editForm = this.formBuilder.group({
      '_id': [this.userId, Validators.required],
      'username': [this.user.username, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(18)])],
      'firstname': [this.user.firstname, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      'lastname': [this.user.lastname, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      'email': [this.user.email, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      'birthday': [this.user.birthday, Validators.required],
      'avatar': [this.user.avatar],
      'profile_facebook': [this.user.profile_facebook],
      'profile_twitter': [this.user.profile_twitter],
      'profile_deviantart': [this.user.profile_deviantart],
      'active_facebook': [this.user.active_facebook],
      'active_twitter': [this.user.active_twitter],
      'active_deviantart': [this.user.active_deviantart],
      'validate': ''
    })
  }

  initChangePassForm() {
    this.changePassForm = this.formBuilder.group({
      'currentPassword': [this.currentPassword, Validators.compose([Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])],
      'newPassword': [this.newPassword, Validators.compose([Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])],
      'passwordRepeated': [this.passwordRepeated, Validators.compose([Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])]
    })
  }

  updateUser() {
    this.userService.updateUser(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.notifier.show({
            type: 'success',
            message: 'CORRECTLY CHANGED!'
          })
          this.userService.getUser(this.userId)
            .pipe(first())
            .subscribe(
              data => {
                localStorage.removeItem('currentUser')
                localStorage.setItem('currentUser', JSON.stringify(data['user']))
              }
            )
        })
  }

  toggleSocialNetworksVisibility(e) {
    let body: any
    if (e.target.id === 'active_facebook') {
      body = {
        userId: this.userId,
        active_facebook: this.active_facebook
      }
    }
    else if (e.target.id === 'active_twitter')
      body = {
        userId: this.userId,
        active_twitter: this.active_twitter
      }
    else if (e.target.id === 'active_deviantart') {
      body = {
        userId: this.userId,
        active_deviantart: this.active_deviantart
      }
    }
    this.userService.setStateOfSocialNetworkButtons(body)
      .pipe(first())
      .subscribe(data => {
          if (data) {
            this.notifier.show({
              type: 'success',
              message: 'SOCIAL NETWORK BUTTON CHANGED!'
            })
            this.userService.getUser(this.userId)
              .subscribe(
                user => {
                  localStorage.removeItem('currentUser')
                  localStorage.setItem('currentUser', JSON.stringify(user['user']))
                })
          }
      })
  }

  loadVariablesToUpdate(post) {
    this.username = post.username
    this.firstname = post.firstname
    this.lastname = post.lastname
    this.email = post.email
    this.birthday = post.birthday
    this.avatar = post.avatar
    this.profile_facebook = post.profile_facebook
    this.profile_twitter = post.profile_twitter
    this.profile_deviantart = post.profile_deviantart
  }

  changePassAddPost(post) {
    this.currentPassword = post.currentPassword
    this.newPassword = post.newPassword
    this.passwordRepeated = post.passwordRepeated

    if (this.checkPassword()) {

      let body = {
        userId: this.userId,
        currentPassword: this.currentPassword,
        newPassword: this.newPassword 
      }

      this.userService.changePassword(body).pipe(first())
        .subscribe(
          data => {
            this.notifier.show({
              type: 'success',
              message: 'PASSWORD CHANGED !'
            })
            $('#changePassModal').modal('toggle')
          },
          err => {
            this.notifier.show({
              type: 'error',
              message: String(err['error'].message).toUpperCase() + ' !'
            })
            this.changePassForm.controls['currentPassword'].setValue('')
          }
        )
    } else {
      this.notifier.show({
        type: 'warning',
        message: 'PASSWORDS DO NOT MATCH !'
      })
    }
  }

  private checkPassword() {
    if (this.newPassword === this.passwordRepeated) return true
    else return false
  }

  desactiveAccount() {
    let body = {
      userId: this.userId,
      active: false
    }
    this.userService.setActiveStatus(body)
      .pipe(first())
      .subscribe(
        data => {
          if (data) {
            this.notifier.show({
              type: 'success',
              message: 'SEE YOU LATER MATE, COME BACK SOON !'
            })
            setTimeout(() => this.clean(), 2000)
          }
        }
      )
  }
  
  private clean() {
    localStorage.clear()
    location.reload()
  }

}
